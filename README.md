# SAM Simulations on Uncertain Futures.

These simulations use code from a recent Jazayeri Model to test how they apply to our study Ambiguous Rhythms.
The simulation code is grabbed from the [JazLab](https://github.com/jazlab/SE_NL_MJ_2020).
In particular the `SAM_simulations.ipynb` file

## How to run.
Have [conda](https://wwww.anaconda.org) installed in your terminal.
Clone this repo move into the folder and type
```
conda env create -f environment.yml
```
then
```
conda activate SAM_simulation
```

And then running the code with python or ipython as you prefer.

I should think that running `run_sim.py` and `run_sims_Ksig.py` would be particularly useful
