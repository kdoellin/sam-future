.PHONY: all header clean code links todos math clean-math contents public serve

all: header code links math todos

header:
	mkdir headers

contents:
	python3 builders/makereadme.py

math:
	python3 builders/fixmath.py

todos: header contents
	python3 builders/maketodos.py

code: header contents
	builders/code2md.sh

links:
	python3 builders/replacecodelinks.py

public: links todos code math
	builders/bookbuild.sh
	mdbook build -d ../public build

serve: links todos code math
	builders/bookbuild.sh
	mdbook serve -d ../public build

clean-math:
	python3 builders/returnmath.py

clean: clean-math
	rm -rf headers
	rm -rf build
	rm -rf public
	rm -f code/*.md
	rm -f SUMMARY.md
	git checkout -- *.md
	git checkout -- reports/
