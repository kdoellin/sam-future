"""Run simple logistic regression and output auc."""
from numpy import concatenate, floor
from sklearn.linear_model import LogisticRegression
# AUC for measuring model performance
from sklearn.metrics import roc_auc_score as score
# StratifiedKFold for cross validation
from sklearn.model_selection import KFold


def logreg(X, y, splits=1, zscore=False):
    """Run simple logistic regression with preferred settings."""
    lrc = LogisticRegression(class_weight="balanced", solver="lbfgs")
    if zscore:
        X = X - X.mean(axis=0, keepdims=True)
        X = X / X.std(axis=0, keepdims=True)
    if splits == 1:
        # train on the training section
        lrc.fit(X=X, y=y)
        # and test on the testing section
        y_pred = lrc.predict_proba(X=X)
        auc = score(y, y_pred[:, 1])

    elif splits == floor(splits):
        if splits == -1:
            splits = len(y)

        # here's our cross-validator
        cv = KFold(n_splits=splits)
        folds = cv.split(X=X, y=y)
        # for each split of the data run the model
        preds = list()
        trues = list()
        for train, test in folds:
            # class_weight doesn't matter here but it would if you had an uneven number
            # of trials in each condition
            # solver is just to avoid annoying FutureWarning
            lrc = LogisticRegression(class_weight="balanced", solver="lbfgs")
            # train on the training section
            lrc.fit(X=X[train], y=y[train])
            # and test on the testing section
            preds.append(lrc.predict_proba(X=X[test])[:, 1])
            trues.append(y[test])
        # save the auc across cross-validation
        ypred = concatenate(preds)
        ytrue = concatenate(trues)
        auc = score(ytrue, ypred)

    else:
        raise ValueError("splits must be an integer of 1 or more")

    return auc
