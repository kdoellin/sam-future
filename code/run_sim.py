"""Run jazlab simulation and plot results."""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from exp_sim import generate_trials, rhy_jitter, soa_jitter
from logreg import logreg
from SAM_sim import PARAMS_DICT, plot_simulation_parallel, simulate_trial

# DONE: Incorporate simulated experimental parameters from BayesFutureModel
# DONE: Run extract y and predict early or late.
# DONE: Look at consistency of Y to SOA and RHY Expectation
sigma = 0.05
initI = 0.77
K = 3.0
duration = 500
dt = PARAMS_DICT["dt"] = 10
normjitter = False

################################################################################
#               Test Jazayeri simulation with signal time course               #
################################################################################
# generate fake signal with ones where each tone is
siglen = 5000
signal = np.zeros([int(siglen / dt), 1])
indx = np.arange(PARAMS_DICT["first_duration"], siglen - duration, duration) / dt
indx += np.random.normal(size=len(indx), scale=0.2 * duration / dt)
signal[indx.astype(int)] = 1
# run trial simulation
ulst, vlst, ylst, Ilst, siglst, ylst2 = simulate_trial(
    ntrials=300,
    nstages=10,
    duration=duration,
    sigma=sigma,
    K=K,
    initI=initI,
    signal=signal,
)
# plot result using jazayeri code
plot_simulation_parallel(ulst, vlst, ylst, Ilst, np.squeeze(siglst), PARAMS_DICT)
fig = plt.gcf()
fig.savefig('../figures/SAM_ExampleTrial.png')
fig.show()

################################################################################
#            Run Simulation on our experiment and get model output             #
################################################################################
# generate our own trials.
ntrials = 200
soa_rnge = [0.4, 0.6]
trials, exp_noise = generate_trials(soa_rnge, ntrials, sig_perc=0.2)
# convert trials to indices using jazayeri format
indices = [np.round(trial * 1000 / dt).astype(int) for trial in trials]
# find maximum value so that all signals are the same length
siglen = np.max([np.max(indic) for indic in indices]) + 1
signals = list()
# generate all signals and collect probe information
probes = dict(index=list(), rhy_jit=list(), soa_jit=list())
for ind, index in enumerate(indices):
    # generate signal
    signal = np.zeros(int(siglen))
    signal[index.astype(int)] = 1
    signals.append(signal)
    # collect probe info
    probes["index"].append(index[-1])
    probes["rhy_jit"].append(rhy_jitter(trials[ind], norm=normjitter) * 1000 / dt)
    probes["soa_jit"].append(soa_jitter(trials[ind], norm=normjitter) * 1000 / dt)
signals = np.array(signals).T
probe_frame = pd.DataFrame(probes)
################################################################################
#                    # run simulation on all trials at once                    #
################################################################################
ulst, vlst, ylst, Ilst, siglst, ylst2 = simulate_trial(
    ntrials=ntrials,
    nstages=None,
    duration=500,
    sigma=sigma,
    K=K,
    initI=initI,
    signal=signals,
)
################################################################################
#    # save ylst and output at probe time and output at expected locations     #
################################################################################
output = np.array(ylst)
probe_frame["probe_out"] = output[probe_frame["index"], np.arange(output.shape[1])]
probe_frame["rhy_out"] = output[
    np.round(probe_frame.index - probe_frame["rhy_jit"]).astype(int),
    np.arange(output.shape[1]),
]
probe_frame["soa_out"] = output[
    np.round(probe_frame.index - probe_frame["soa_jit"]).astype(int),
    np.arange(output.shape[1]),
]

################################################################################
#         Calculate Model Behavior with Regard to SOA and RHY Methods          #
################################################################################
print(probe_frame["rhy_out"].std(), probe_frame["soa_out"].std())
X = np.array(probe_frame["probe_out"]).reshape(-1, 1)
y = np.sign(probe_frame["rhy_jit"])
auc = logreg(X, y)
print(f"RHY (AUC = {auc})")
y = np.sign(probe_frame["soa_jit"])
auc = logreg(X, y)
print(f"SOA (AUC = {auc})")
