"""Test frequency effects of ambiguous simulation."""
import matplotlib.pyplot as plt
import numpy as np
import statsmodels.api as sm
from scipy.stats import chi2
from statsmodels.tools.sm_exceptions import PerfectSeparationError

from exp_sim import generate_trials, signals_from_trials
from SAM_sim import COLS, PARAMS_DICT, simulate_trial

# DONE: Run polynomial fit on the outcomes to see which polynomial fits best.


################################################################################
#                      Set parameters of basic simulation                      #
################################################################################
savefigs = True
sigmas = np.logspace(-2, 0, 20)
Ks = np.logspace(0.2, 0.7, 5)
initI = 0.77
dt = PARAMS_DICT["dt"]
ntrials = 500
soa_rnges = [[0.7, 1.0], [0.4, 0.6], [0.21, 0.29]]


################################################################################
#                                Run simulation                                #
################################################################################
Ksig_freq = list()
exp_freq = list()
labels = list()
for soa_rnge in soa_rnges:
    label = f"{1/np.mean(soa_rnge):1.1f} Hz"
    labels.append(label)
    print(f"Freq = {label}")
    Ksig_outs = list()
    exp_Ksig = list()
    trials, exp_noise = generate_trials(soa_rnge, ntrials, sig_perc=0.2)
    signals, probe_frame = signals_from_trials(trials, dt)
    for K in Ks:
        sig_outs = list()
        exp_sig = list()
        for sigma in sigmas:
            print("K = {K:1.2f}, sigma = {sigma:1.3f}".format(K=K, sigma=sigma))
            # generate new signals for each experiment
            ulst, vlst, ylst, Ilst, siglst, ylst2 = simulate_trial(
                ntrials=ntrials,
                nstages=None,
                duration=500,
                sigma=sigma,
                K=K,
                initI=initI,
                signal=signals,
            )
            # take y as output
            output = np.array(ylst)
            # record the y value at probe time, rhy time and soa time
            probe_frame["probe_out"] = output[
                probe_frame["index"], np.arange(output.shape[1])
            ]
            # GLM logit regresion
            # predicted response of the model based on decision threshold
            y = (probe_frame["probe_out"] >= PARAMS_DICT["y0"]).astype(int)
            slopes = list()
            exp_stats = list()
            # for each jitter type
            for jitter in ["rhy_jit", "soa_jit"]:
                if len(np.unique(y)) == 1:
                    slopes.append(np.nan)
                    continue
                X = probe_frame[jitter] * dt / 1000
                # normalize by average duration of that trial
                X /= np.array([np.mean(np.diff(trial)) for trial in trials])
                # add constant
                X = sm.add_constant(X)
                # fit the model
                mod = sm.GLM(y, X, family=sm.families.Binomial())
                try:
                    res = mod.fit()
                except PerfectSeparationError:
                    print("Perfect Separation Error...")
                    slopes.append(np.nan)
                else:
                    dev_diff = res.null_deviance - res.deviance
                    chip = 1 - chi2.cdf(dev_diff, 1)
                    # take slope parameter if it the fit is significant
                    if chip < 0.05:
                        slopes.append(res.params[1])
                    else:
                        slopes.append(np.nan)
                # get variance and mean of output at expectation
                exp_out = output[
                    np.round(probe_frame["index"] - probe_frame[jitter]).astype(int),
                    np.arange(output.shape[1]),
                ]
                exp_stats.append([np.mean(exp_out), np.std(exp_out)])
            exp_sig.append(exp_stats)
            sig_outs.append(slopes)
        Ksig_outs.append(sig_outs)
        exp_Ksig.append(exp_sig)
    Ksig_freq.append(Ksig_outs)
    exp_freq.append(exp_Ksig)


fig_dir = "../figures/"
from matplotlib.patches import Rectangle

###############################################################################
#                        plot differences by frequency                        #
###############################################################################
fig_df, ax_df = plt.subplots()
alldiffs = np.diff(Ksig_freq, axis=-1).reshape([3, -1]).T
xnoise = np.random.normal(size=alldiffs.shape[0], scale=0.1)[:, None]
x = np.arange(alldiffs.shape[1])[None, :] + 1
ax_df.set_prop_cycle(color=COLS)
for i, ad in enumerate(alldiffs.T):
    i = i + 1
    ad = ad[np.logical_not(np.isnan(ad))]
    mu = np.mean(ad)
    std = np.std(ad)
    width = 0.35
    ax_df.add_patch(
        Rectangle(
            (i - width, mu - std),
            2 * width,
            2 * std,
            facecolor="gray",
            edgecolor="black",
        )
    )
    ax_df.plot((i - width, i + width), (mu, mu), color="C1")
# bp = ax_df.boxplot(
#     [ad[np.logical_not(np.isnan(ad))] for ad in alldiffs.T],
#     showfliers=False,
#     showmeans=True,
#     meanline=True,
#     whis=0,
#     medianprops={"linewidth": 0},
#     meanprops={"color": "C1", "ls": "-"},
#     widths=0.6,
#     patch_artist=True,
#     boxprops=dict(facecolor=[0.6, 0.6, 0.6], zorder=-1),
# )
ax_df.plot(x + xnoise, alldiffs, ".", ms=15, mec="k")
ax_df.set_ylabel(r"$\rm {Slope_{ABS}-Slope_{REL}}$")
ax_df.set_xticks(np.squeeze(x))
ax_df.set_xticklabels(labels)
ax_df.axhline(0, color="k", ls="--")
ax_df.set_ylim([-15, 30])
fig_df.tight_layout()
fig_df.show()


################################################################################
#             Plot model differences by parameter and performance              #
################################################################################
printdata = False
xRange = [0, 44]
# plot diff by performance
fig_dbyp, ax = plt.subplots()
fig_aic, ax_aic = plt.subplots(len(Ksig_freq), figsize=(2, 4))
nPoly = 3
best_models = list()
diffs = []
perfs = []
for ind, Ksig_outs in enumerate(Ksig_freq):
    slope_diffs = np.squeeze(np.diff(Ksig_outs, axis=2))
    slope_perfs = np.sum(Ksig_outs, axis=2)
    if printdata:
        print(f"{labels[ind]}:")
        print("Perf\tDiff")
    for sc_ind, (perf, diff) in enumerate(
        zip(np.sum(Ksig_outs, axis=2).flatten(), slope_diffs.flatten())
    ):
        hdl = ax.plot(
            perf,
            diff,
            ".",
            ms=15,
            color=COLS[ind],
            zorder=np.random.uniform() * 100,
            mec="k",
        )
        if printdata:
            print(f"{perf},\t{diff}")
        if sc_ind == 0:
            hdl[0].set_label(labels[ind])
    models = list()
    diff = slope_diffs[np.logical_not(np.isnan(slope_diffs))]
    perf = slope_perfs[np.logical_not(np.isnan(slope_diffs))]
    diffs = np.hstack([diffs, diff])
    perfs = np.hstack([perfs, perf])
    for nP in range(1, nPoly + 1):
        # Fit a polynomial of specificied degree (nP) to the data and return minimum aic
        res = sm.OLS(endog=diff, exog=np.vander(perf, nP + 1)[:, :-1]).fit()
        models.append(res)
    aics = [mod.aic for mod in models]
    ax_aic[ind].bar(
        np.arange(len(aics)) + 1,
        aics - min(aics),
        bottom=min(aics),
        color=COLS[ind],
        edgecolor="k",
    )
    ax_aic[ind].set_title(labels[ind])
    # get min aic model and plot the predictions
    bM = np.argmin(aics)
    res = models[bM]
    # x = np.linspace(*xRange, 101)
    # preds = res.get_prediction(np.vander(x, bM + 2)[:, :-1])
    # ax.plot(x, preds.predicted_mean, color=COLS[ind], zorder=101)
    # ax.fill_between(x, *preds.conf_int().T, color=COLS[ind], alpha=0.4, zorder=101)
    best_models.append(res)
ax.set_xlabel(r"$\rm {Slope_{ABS}+Slope_{REL}}$")
ax.set_ylabel(r"$\rm {Slope_{ABS}-Slope_{REL}}$")
ax.set_ylim([-15, 30])
ax.set_xlim(xRange)
ax.axhline(0, color='k', ls="--")
ax.legend()
models = list()
for nP in range(1, nPoly + 1):
    # Fit a polynomial of specificied degree (nP) to the data and return minimum aic
    res = sm.OLS(endog=diffs, exog=np.vander(perfs, nP + 1)[:, :-1]).fit()
    models.append(res)
aics = [mod.aic for mod in models]
# get min aic model and plot the predictions
bM = np.argmin(aics)
res = models[bM]
xlim = ax.get_xlim()
x = np.linspace(*xlim, 101)
preds = res.get_prediction(np.vander(x, bM + 2)[:, :-1])
ax.plot(x, preds.predicted_mean, color="k", zorder=101)
ax.fill_between(x, *preds.conf_int().T, color="k", alpha=0.4, zorder=101)
ax.set_xlim(xlim)
fig_dbyp.tight_layout()
fig_dbyp.show()
fig_aic.tight_layout()
fig_aic.show()

# plot model parameters
numPlots = max([len(mod.params) for mod in best_models])
fig_pars, axs = plt.subplots(nrows=numPlots, figsize=(3, 8))
fig_fit, ax_fit = plt.subplots()
for ind, mod in enumerate(best_models):
    par = mod.params
    conf = mod.conf_int()
    for p, ci, ax in zip(np.flip(par), np.flip(conf), axs):
        ax.plot([ind, ind], ci, lw=3, color=COLS[ind])
        ax.plot(ind, p, "+", color=COLS[ind])
    x = np.linspace(0, 5, 101)
    preds = mod.get_prediction(np.vander(x, len(par) + 1)[:, :-1])
    ax_fit.plot(x, preds.predicted_mean, color=COLS[ind])
    ax_fit.fill_between(x, *preds.conf_int().T, color=COLS[ind], alpha=0.4)
for ax in axs:
    ax.set_xticks(np.arange(len(best_models)))
    ax.set_xticklabels(labels)
    ax.axhline(0, ls="--", color="k", zorder=-1)
fig_pars.tight_layout()
fig_pars.show()
fig_fit.show()

# plot slope diffs by parameters
fig_dbypar, axs = plt.subplots(ncols=3, figsize=(15, 5))
for ind, (Ksig_outs, ax) in enumerate(zip(Ksig_freq, axs)):
    slope_diffs = np.squeeze(np.diff(Ksig_outs, axis=2))
    vmax = np.nanmax(np.abs(slope_diffs))
    # cmax = np.nanmax(np.abs(slope_diffs))
    im = ax.pcolormesh(
        sigmas, Ks, slope_diffs, cmap="RdBu_r", vmin=-vmax, vmax=vmax, shading="auto"
    )
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_title(labels[ind])
    cb = fig_dbypar.colorbar(im, ax=ax)
    cb.set_label("SOA - RHY", rotation=270)
    cb.ax.set_title("SOA - RHY")
    if ind == 1:
        ax.set_xlabel(r"Model Noise ($\sigma$)")
    elif ind == 0:
        ax.set_ylabel("Learning Parameter (K)")
fig_dbypar.suptitle("Discriminability (Slope)")
fig_dbypar.tight_layout(rect=[0, 0.03, 1, 0.95])
fig_dbypar.show()

################################################################################
#                                 Save Figures                                 #
################################################################################
if savefigs:
    fig_dbyp.savefig(fig_dir + "SAM_Freq_diff_by_perf.svg")
    fig_dbypar.savefig(fig_dir + "SAM_Freq_diff_by_params.png")
    fig_df.savefig(fig_dir + "SAM_Freq_diff.svg")
