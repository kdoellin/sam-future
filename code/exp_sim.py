"""Simulate Experimental details."""
import numpy as np
import pandas as pd


def soa_jitter(trial, norm=False):
    """Return jitter using the SOA (mean) method."""
    zero = trial[-2] + np.mean(np.diff(trial[:-1]))

    normalize = np.mean(np.diff(trial[:-1])) if norm else 1
    return (trial[-1] - zero) / normalize


def rhy_jitter(trial, norm=False):
    """Return jitter using the Rhythm (linear regression) method."""
    x = np.arange(len(trial))
    x = np.vstack([x, np.ones_like(x)])
    m = np.linalg.inv(x[:, :-1] @ x[:, :-1].T) @ (x[:, :-1] @ trial[:-1])
    zero = x[:, -1] @ m

    normalize = np.mean(np.diff(trial[:-1])) if norm else 1
    return (trial[-1] - zero) / normalize


def generate_trials(
    soa_range,
    num_trials,
    sig_perc=0.2,
    probe_perc=1.2,
    tone_perc=0.4,
    num_tones=np.arange(8, 11),
):
    """Generate trials replicating the previous matlab function.

    Args:
        soa_range (list): sequence of length 2, the upper and lower limits of mean
            interval for each trial
        num_trials (int): how many trials to generate.
        sig_perc (float): default is 0.2, standard deviation for time jitter as a
            percentage of mean soa_range
        probe_perc (float): default is 1.2, the total range of times for the probe
            distribution as a percentage of mean soa_rnage
        tone_perc (float): default is 0.4, length of tone as a percentage of mean
            soa_range
        num_tones (list[int]): possible number of tones for each trial to be drawn from
            randomly.

    Returns:
        trials (list[list[float]]): Outer list contains trials, inner list contains
            times of each tone within the trial.
        exp_noise (float): the true sigma of jitter in the task.

    """
    from scipy.stats import expon, norm

    exp_noise = np.mean(soa_range) * sig_perc
    probe_range = np.mean(soa_range) * probe_perc
    tone_len = np.mean(soa_range) * tone_perc

    trials = list()
    for _ in range(num_trials):
        num_tone = np.random.choice(num_tones)
        soa = np.random.random() * np.abs(np.diff(soa_range)) + np.min(soa_range)

        flag = 0
        keepsign = False
        prevjit = 0
        while flag == 0:
            noises = norm.rvs(size=num_tone, scale=exp_noise)
            # reshape noises so that last one is one of the longer noise bumps
            half = num_tone // 2
            tmp = np.sort(np.abs(noises))
            tmp = tmp[half:]
            tmp = np.random.choice(tmp)
            n_index = np.abs(noises) == tmp
            last_soa = noises[n_index]
            noises[n_index] = noises[-1]
            noises[-1] = last_soa

            cleanSOAs = np.zeros(num_tone)
            cleanSOAs[1:] += soa
            cleanTones = np.cumsum(cleanSOAs)
            shiftedTones = cleanTones + noises

            if keepsign:
                liljit = prevjit
            else:
                liljit = (np.random.random() - 0.5) * probe_range

            probeT = cleanTones[-1] + soa + liljit
            trial = np.concatenate([shiftedTones, probeT])
            nsySOAs = np.diff(trial)
            if np.min(nsySOAs) > tone_len:
                flag = 1
            elif probeT < (shiftedTones[-1] + tone_len):
                keepsign = True
                prevjit = (expon.rvs(scale=0.2) - 1) * probe_range / 2
            else:
                keepsign = False
        trials.append(trial)

    return trials, exp_noise


def signals_from_trials(trials, dt):
    """Generate signals based on trial info.

    Args:
        trials (list[list[float]]): time point of each tone in seconds
        dt (float): stepsize of each timepoint in milliseconds

    Returns:
        signals (2-d array): signals generate with time as 0-axis and len(trials) as
            1-axis
        probe_frame (pandas.DataFrame): DataFrame containing relevant information about
            probe tone
    """
    # convert trials to indices using jazayeri format
    indices = [np.round(trial * 1000 / dt).astype(int) for trial in trials]
    # find maximum value so that all signals are the same length
    siglen = np.max([np.max(indic) + np.mean(np.diff(indic)) for indic in indices]) + 1
    signals = list()
    # generate all signals and collect probe information
    probes = dict(index=list(), rhy_jit=list(), soa_jit=list())
    for ind, index in enumerate(indices):
        # generate signal
        signal = np.zeros(int(siglen))
        # don't include the probe tone in the signal so that we can see where
        # expectation would have been if the tone comes before.
        signal[index[:-1].astype(int)] = 1
        signals.append(signal)
        # collect probe info
        probes["index"].append(index[-1])
        probes["rhy_jit"].append(rhy_jitter(trials[ind], norm=False) * 1000 / dt)
        probes["soa_jit"].append(soa_jitter(trials[ind], norm=False) * 1000 / dt)
    signals = np.array(signals).T
    probe_frame = pd.DataFrame(probes)
    return signals, probe_frame
