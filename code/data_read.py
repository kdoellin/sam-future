"""Read in behavioral data."""
from pathlib import Path

import numpy as np
import pandas as pd
import statsmodels.api as sm
from scipy.io import loadmat
from scipy.stats import norm

from exp_sim import signals_from_trials
from SAM_sim import PARAMS_DICT, simulate_trial


def read_sub(subpath):
    """Return data from subject."""
    if isinstance(subpath, str):
        subpath = Path(subpath)
    elif not isinstance(subpath, Path):
        raise TypeError(
            "subpath must be type str or Path, not {}".format(type(subpath))
        )
    csvfile = subpath.joinpath(f"{subpath.name}_data.csv")
    matfile = subpath.joinpath(f"{subpath.name}_time.mat")
    if csvfile.exists():
        subdata = pd.read_csv(csvfile.as_posix())
    else:
        FileNotFoundError(f"{subpath.as_posix()} is missing csvfile.")

    if matfile.exists():
        subtime = loadmat(matfile.as_posix())
    else:
        FileNotFoundError(f"{subpath.as_posix()} is missing matfile.")

    return subdata, subtime["timing"]


def fit_data_dp(params, signals, probe_frame, resp):
    """Fit parameters of model to subjects."""
    y0, K = params
    reps = 1
    _, _, ylst, _, _, _ = simulate_trial(
        ntrials=signals.shape[1] * reps,
        nstages=None,
        duration=500,
        sigma=0,
        K=K,
        initI=0.77,
        signal=np.tile(signals, reps),
        y0=y0,
    )
    output = np.array(ylst)
    probe_out = output[np.tile(probe_frame["index"], reps), np.arange(output.shape[1])]
    late_pred = probe_out > y0
    late_true = np.tile(resp > 2, reps)
    hr = np.sum(np.logical_and(late_pred, late_true))
    ms = np.sum(np.logical_and(~late_pred, late_true))
    fa = np.sum(np.logical_and(late_pred, ~late_true))
    cr = np.sum(np.logical_and(~late_pred, ~late_true))
    # correction for perfect scores.
    ms = ms if ms > 0 else 0.5
    fa = fa if fa > 0 else 0.5
    # calculate dprime
    dp = norm.ppf(hr / (hr + ms)) - norm.ppf(fa / (fa + cr))
    print(f"{params}: {dp}")
    return -dp


def fit_data_nll(params, signals, probe_frame, resp):
    """Fit parameters of model to subjects."""
    y0, K = params
    reps = 1
    _, _, ylst, _, _, _ = simulate_trial(
        ntrials=signals.shape[1] * reps,
        nstages=None,
        duration=500,
        sigma=0,
        K=K,
        initI=0.77,
        signal=np.tile(signals, reps),
        y0=y0,
    )
    output = np.array(ylst)
    probe_out = output[np.tile(probe_frame["index"], reps), np.arange(output.shape[1])]
    X = probe_out - y0
    y = (resp > 2).astype(int)
    # fit the model
    mod = sm.GLM(y, X, family=sm.families.Binomial())
    res = mod.fit()
    print(f"{params}: {res.llf}")
    return -res.llf
