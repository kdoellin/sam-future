"""Fit SAM_simulation to individual participants."""
import os
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import brute, dual_annealing, fmin, minimize

from data_read import fit_data_nll as fit_data
from data_read import read_sub
from exp_sim import signals_from_trials
from SAM_sim import PARAMS_DICT, simulate_trial


def callbackF(Xi, f, con):
    global Ngfeval
    print(f"{Ngfeval: d} global: {Xi} {f: 3.6f}")
    Ngfeval += 1


def callback(Xi):
    global Nlfeval
    print(f"{Nlfeval: d} local: {Xi}")
    Nlfeval += 1


def fit_sub_par(subpath):
    """Fitting function for each subject."""
    data, timing = read_sub(subpath)
    timing = [tim[~np.isnan(tim)] for tim in timing]
    signals, probe_frame = signals_from_trials(timing, PARAMS_DICT["dt"])
    res = minimize(fit_data, [y0, K, sigma], args=(signals, probe_frame, data.Resp))
    return res


if __name__ == "__main__":
    __spec__ = None
    fig_dir = Path("../figures/subfits/")
    fig_dir.mkdir(exist_ok=True)
    sigma = 0.01
    K = 5
    initI = 0.77
    y0 = PARAMS_DICT["y0"]

    subpaths = [
        Path(subpath[0]) for subpath in os.walk("data") if subpath[0].count(os.sep) == 2
    ]
    slopes = []
    subparams = list()
    for subpath in subpaths[:1]:
        data, timing = read_sub(subpath.as_posix())
        timing = [tim[~np.isnan(tim)] for tim in timing]
        signals, probe_frame = signals_from_trials(timing, PARAMS_DICT["dt"])
        Ngfeval = Nlfeval = 0
        args = (signals, probe_frame, data.Resp)
        xparams, fval, grid, fgrid = brute(
            fit_data,
            (slice(0.4, 0.84, 0.04), slice(0.0, 10, 0.25)),
            args=args,
            full_output=True,
            finish=fmin,
            workers=-1,
            disp=True,
        )
        fig = plt.figure()
        plt.pcolormesh(*grid, -fgrid, shading="nearest", cmap=plt.cm.cividis)
        plt.plot(*xparams, "*", color="w")
        plt.xlabel("$y_0$ (decision threshold)")
        plt.ylabel("$K$ (learning parameter)")
        plt.title(f"Model Fit of {subpath.name}, {subpath.parts[-2]}")
        cb = plt.colorbar()
        cb.ax.set_title("d'")
        fig.savefig(fig_dir.joinpath("_".join(subpath.parts[-2:])))
