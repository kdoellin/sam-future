"""Run jazlab simulation and plot results."""
import matplotlib.pyplot as plt
import numpy as np
import statsmodels.api as sm
from statsmodels.tools.sm_exceptions import PerfectSeparationError
import statsmodels.formula.api as smf

from exp_sim import generate_trials, signals_from_trials
from SAM_sim import COLS, PARAMS_DICT, simulate_trial

savefigs = False
sigmas = np.logspace(-2, 0, 10)
Ks = np.logspace(-1, 1, 10)
initI = 0.77
normjitter = False
dt = PARAMS_DICT["dt"]

################################################################################
#                         Make Experiment Trial Setup                          #
################################################################################
# generate our own trials.
ntrials = 500
soa_rnge = [0.4, 0.6]
trials, exp_noise = generate_trials(soa_rnge, ntrials, sig_perc=0.2)
signals, probe_frame = signals_from_trials(trials, dt)

################################################################################
#           Grid Search of Parameter effect on SOA vs RHY preference           #
################################################################################
Ksig_outs = list()
exp_Ksig = list()
for K in Ks:
    sig_outs = list()
    exp_sig = list()
    for sigma in sigmas:
        print("K = {K}, sigma = {sigma}".format(K=K, sigma=sigma))
        # simulate the trial
        ulst, vlst, ylst, Ilst, siglst, ylst2 = simulate_trial(
            ntrials=ntrials,
            nstages=None,
            duration=500,
            sigma=sigma,
            K=K,
            initI=initI,
            signal=signals,
        )
        # take y as output
        output = np.array(ylst)
        # record the y value at probe time, rhy time and soa time
        probe_frame["probe_out"] = output[
            probe_frame["index"], np.arange(output.shape[1])
        ]
        # GLM logit regresion
        # predicted response of the model based on decision threshold
        y = (probe_frame["probe_out"] >= PARAMS_DICT["y0"]).astype(int)
        slopes = list()
        exp_stats = list()
        # for each jitter type
        for jitter in ["rhy_jit", "soa_jit"]:
            if len(np.unique(y)) == 1:
                slopes.append(np.nan)
                continue
            # add constant
            X = sm.add_constant(probe_frame[jitter] * dt / 1000)
            # fit the model
            mod = sm.GLM(y, X, family=sm.families.Binomial())
            try:
                res = mod.fit()
            except PerfectSeparationError:
                print("Perfect Separation Error...")
                slopes.append(np.nan)
            else:
                # take slope parameter
                slopes.append(res.params[1])
            # get variance and mean of output at expectation
            exp_out = output[
                np.round(probe_frame["index"] - probe_frame[jitter]).astype(int),
                np.arange(output.shape[1]),
            ]
            exp_stats.append([np.mean(exp_out), np.std(exp_out)])
        exp_sig.append(exp_stats)
        sig_outs.append(slopes)
    Ksig_outs.append(sig_outs)
    exp_Ksig.append(exp_sig)

################################################################################
#            Plot Model Behavior For Difference between RHY and SOA            #
################################################################################
# set figure directory
fig_dir = "../figures/"
slope_diffs = np.squeeze(np.diff(Ksig_outs, axis=2))
cmax = np.max(np.abs(slope_diffs))
fig_pbyp, ax = plt.subplots()
im = ax.pcolormesh(
    sigmas, Ks, slope_diffs, cmap="RdBu_r", vmin=-cmax, vmax=cmax, shading="auto"
)
ax.set_xlabel(r"Model Noise ($\sigma$)")
ax.set_ylabel("Learning Parameter (K)")
ax.set_title("Discriminability (Slope)")
ax.set_xscale("log")
ax.set_yscale("Log")
cb = fig_pbyp.colorbar(im, ax=ax)
cb.set_label("SOA - RHY\nSOA is Positive", rotation=90)
plt.tight_layout()
fig_pbyp.show()

fig_dbyp, ax = plt.subplots()
outs = np.array(Ksig_outs).reshape([-1, 2])
ax.plot(*outs.T, ".", ms=15, markeredgecolor='k', mfc=[.6, .6, .6], lw=1, alpha=.7)
ax.set_xlabel("Rhythm Performance")
ax.set_ylabel("Interval Performance")
ax.axline([0, 0], slope=1, ls="--", color='k')
nPoly = 4
models = list()
for nP in range(nPoly):
    eq = 'soa ~ 0 + ' + ' + '.join([f'np.power(rhy, {p})' for p in range(1, nP+2)])
    mdf = smf.ols(eq, data=dict(rhy=outs[:, 0], soa=outs[:, 1])).fit()
    models.append(mdf)
bm = np.argmin([mod.aic for mod in models])
x = np.linspace(0, np.max(np.array(Ksig_outs)[..., 0]), 101)
pred = models[bm].get_prediction(dict(rhy=x))
ax.plot(x, pred.predicted_mean, color=[.6, .6, .6], lw=2)
ax.fill_between(x, *pred.conf_int().T, color=[.6, .6, .6], alpha=.4)
plt.tight_layout()
fig_dbyp.show()

################################################################################
#             Plot model consistency to rhy and soa expected times             #
################################################################################
var = 1 / np.array(exp_Ksig)
var_diff = np.squeeze(np.diff(var, axis=2))[:, :, -1]
cmax = np.max(np.abs(var_diff))
fig_vbypar, ax = plt.subplots()
im = ax.pcolormesh(
    sigmas, Ks, var_diff, cmap="RdBu_r", vmin=-cmax, vmax=cmax, shading="auto"
)
ax.set_xlabel(r"Model Noise ($\sigma$)")
ax.set_ylabel("Learning Parameter (K)")
ax.set_title(r"Expectation Consistency ($\frac{1}{\sigma_y}$)")
ax.set_xscale("log")
ax.set_yscale("Log")
cb = fig_vbypar.colorbar(im, ax=ax)
cb.set_label("SOA - RHY\nSOA is Positive", rotation=90)
plt.tight_layout()
fig_vbypar.show()

if savefigs:
    fig_vbypar.savefig(fig_dir + "SAM_diff_by_std.png")
    fig_dbyp.savefig(fig_dir + "SAM_diff_by_perf.png")
    fig_pbyp.savefig(fig_dir + "SAM_diff_by_params.png")
