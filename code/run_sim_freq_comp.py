"""Testing for the difference between 4 Hz and 2 Hz."""
import matplotlib.pyplot as plt
import numpy as np

from exp_sim import generate_trials, signals_from_trials
from SAM_sim import PARAMS_DICT, plot_simulation_parallel, simulate_trial

# DONE: Incorporate simulated experimental parameters from BayesFutureModel
# DONE: Run extract y and predict early or late.
# DONE: Look at consistency of Y to SOA and RHY Expectation
fig_dir = "../figures/"
sigma = 0.001
initI = 0.77
K = 3.0
duration = 500
dt = PARAMS_DICT["dt"] = 10
normjitter = False

################################################################################
#             Test Jazayeri model with same stimulus at two rates              #
################################################################################
ntrials = 100
# use 1.2 Hz case
soa_rnge = [0.7, 1.0]
trials, exp_noise = generate_trials(soa_rnge, 1, sig_perc=0.2)
# divide the trial info by a certain number to compare a faster verion of the same trial
for div in [1, 3]:
    # either do or don't quicken the trial.
    trial_test = [trial / div for trial in trials]
    # generate signal
    signal, probe_frame = signals_from_trials(trial_test, dt)
    # run trial simulation
    ulst, vlst, ylst, Ilst, siglst, ylst2 = simulate_trial(
        ntrials=ntrials,
        nstages=10,
        duration=duration,
        sigma=sigma,
        K=K,
        initI=initI,
        signal=signal,
    )
    # plot result using jazayeri code
    plot_simulation_parallel(ulst, vlst, ylst, Ilst, np.squeeze(siglst), PARAMS_DICT)
    # add label
    fig = plt.gcf()
    label = f"{div / np.mean(soa_rnge): 1.1f} Hz"
    fig.suptitle(label)
    fig.show()

################################################################################
#                 Show plateau of model at different currents                  #
################################################################################
ntrials = 1
signal = np.zeros_like(signal)
fig, axs = plt.subplots(ncols=3, figsize=(15, 5))
for initI in [0.77, 0.65]:
    # run trial simulation
    ulst, vlst, ylst, Ilst, siglst, ylst2 = simulate_trial(
        ntrials=ntrials,
        nstages=10,
        duration=duration,
        sigma=sigma,
        K=K,
        initI=initI,
        signal=signal,
    )
    for ax, lst, var in zip(axs, [ulst, vlst, ylst], ["u", "v", "y"]):
        ax.plot(np.squeeze(lst), label=f"{initI:.2f}", lw=2)
        ax.set_title(var)
ax.axhline(PARAMS_DICT["y0"], color="k", ls="--", label="Threshold")
ax.legend()
fig.show()
fig.savefig(fig_dir + "SAM_plateau_by_current.png")

# The data shows that changing I changes the dynamic range of v and therefore y. This
# could be the reason why we see a different result in method across frequencies in the
# model. If true, it means that we should be able to manipulate SOA vs Rhythm by
# changing the y0 threshold.
