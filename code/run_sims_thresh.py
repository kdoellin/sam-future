"""Test performance against different thresholds."""
import matplotlib.pyplot as plt
import numpy as np
import statsmodels.api as sm
from statsmodels.tools.sm_exceptions import PerfectSeparationError

from exp_sim import generate_trials, signals_from_trials
from SAM_sim import COLS, PARAMS_DICT, simulate_trial

################################################################################
#                      Set parameters of basic simulation                      #
################################################################################
savefigs = True
sigma = 0.01
y0s = np.linspace(0.5, 0.95, 46)
Ks = np.logspace(-0.5, 1, 20)
initI = 0.77
dt = PARAMS_DICT["dt"]
ntrials = 500
soa_rnges = [[0.21, 0.29], [0.4, 0.6], [0.7, 1.0]]


###############################################################################
#                      Run Simulation through Ks and y0s                      #
###############################################################################
Ksig_freq = list()
exp_freq = list()
labels = list()
for soa_rnge in soa_rnges:
    label = f"{1/np.mean(soa_rnge):1.1f} Hz"
    labels.append(label)
    # generate new signals for each experiment
    trials, exp_noise = generate_trials(soa_rnge, ntrials, sig_perc=0.2)
    signals, probe_frame = signals_from_trials(trials, dt)
    print(f"Freq = {label}")
    Ksig_outs = list()
    exp_Ksig = list()
    for K in Ks:
        sig_outs = list()
        exp_sig = list()
        for y0 in y0s:
            PARAMS_DICT["y0"] = y0
            print(f"K = {K:1.2f}, y0 = {y0:1.2f}")
            ulst, vlst, ylst, Ilst, siglst, ylst2 = simulate_trial(
                ntrials=ntrials,
                nstages=None,
                duration=500,
                sigma=sigma,
                K=K,
                initI=initI,
                signal=signals,
            )
            # take y as output
            output = np.array(ylst)
            # record the y value at probe time, rhy time and soa time
            probe_frame["probe_out"] = output[
                probe_frame["index"], np.arange(output.shape[1])
            ]
            # GLM logit regresion
            # predicted response of the model based on decision threshold
            y = (probe_frame["probe_out"] >= PARAMS_DICT["y0"]).astype(int)
            slopes = list()
            exp_stats = list()
            # for each jitter type
            for jitter in ["rhy_jit", "soa_jit"]:
                if len(np.unique(y)) == 1:
                    slopes.append(np.nan)
                    continue
                X = probe_frame[jitter] * dt / 1000
                # normalize by average duration of that trial
                X /= np.array([np.mean(np.diff(trial)) for trial in trials])
                # add constant
                X = sm.add_constant(probe_frame[jitter] * dt / 1000)
                # fit the model
                mod = sm.GLM(y, X, family=sm.families.Binomial())
                try:
                    res = mod.fit()
                except PerfectSeparationError:
                    print("Perfect Separation Error...")
                    slopes.append(np.nan)
                else:
                    # take slope parameter
                    slopes.append(res.params[1])
                # get variance and mean of output at expectation
                exp_out = output[
                    np.round(probe_frame["index"] - probe_frame[jitter]).astype(int),
                    np.arange(output.shape[1]),
                ]
                exp_stats.append([np.mean(exp_out), np.std(exp_out)])
            exp_sig.append(exp_stats)
            sig_outs.append(slopes)
        Ksig_outs.append(sig_outs)
        exp_Ksig.append(exp_sig)
    Ksig_freq.append(Ksig_outs)
    exp_freq.append(exp_Ksig)

###############################################################################
#                              Plot the results.                              #
###############################################################################
# plot diff by performance
fig_dir = "../figures/"
fig_dbyp, ax = plt.subplots()
for ind, Ksig_outs in enumerate(Ksig_freq):
    slope_diffs = np.squeeze(np.diff(Ksig_outs, axis=2))
    ax.plot(
        np.mean(Ksig_outs, axis=2).flatten(),
        slope_diffs.flatten(),
        ".",
        ms=15,
        color=COLS[ind],
        label=labels[ind],
    )
ax.set_xlabel("Overall Performance")
ax.set_ylabel("SOA - RHY\nSOA is Positive")
ax.axhline(0, color=COLS[1], ls="--")
plt.legend()
plt.tight_layout()
fig_dbyp.show()

# plot slope diffs by parameters
fig_dbypar, axs = plt.subplots(ncols=3, figsize=(15, 5))
vmax = 50
for ind, (Ksig_outs, ax) in enumerate(zip(Ksig_freq, axs)):
    slope_diffs = np.squeeze(np.diff(Ksig_outs, axis=2))
    # cmax = np.nanmax(np.abs(slope_diffs))
    im = ax.pcolormesh(
        y0s, Ks, slope_diffs, cmap="RdBu_r", vmin=-vmax, vmax=vmax, shading="auto"
    )
    ax.set_xscale("linear")
    ax.set_yscale("log")
    ax.set_title(labels[ind])
    cb = fig_dbypar.colorbar(im, ax=ax)
    cb.ax.set_title("SOA - RHY")
    if ind == 1:
        ax.set_xlabel(r"Decision Threshold ($y_0$)")
    elif ind == 0:
        ax.set_ylabel("Learning Parameter (K)")
fig_dbypar.suptitle("Discriminability (Slope)")
fig_dbypar.tight_layout(rect=[0, 0.03, 1, 0.95])
fig_dbypar.show()

################################################################################
#                                 Save Figures                                 #
################################################################################
if savefigs:
    fig_dbyp.savefig(fig_dir + "SAM_Thresh_diff_by_perf.png")
    fig_dbypar.savefig(fig_dir + "SAM_Thresh_diff_by_params.png")
