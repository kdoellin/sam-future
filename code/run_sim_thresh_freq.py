"""Test performance against different thresholds."""
import matplotlib.pyplot as plt
import numpy as np
import statsmodels.api as sm
from statsmodels.tools.sm_exceptions import PerfectSeparationError

from exp_sim import generate_trials, signals_from_trials
from SAM_sim import COLS, PARAMS_DICT, simulate_trial

################################################################################
#                      Set parameters of basic simulation                      #
################################################################################
savefigs = True
fig_dir = "../figures/"
sigma = 0.01
y0s = np.linspace(0.3, 0.9, 31)
Ks = np.logspace(np.log10(0.5), np.log10(5), 4)
initI = 0.77
dt = PARAMS_DICT["dt"]
ntrials = 500
freqs = np.linspace(0.5, 4.5, 21)
soa_rnges = 1.0 / freqs[:, None] + 0.2 / freqs[:, None] * np.array([[-1, 1]])

###############################################################################
#                      Run Simulation through Ks and y0s                      #
###############################################################################
y0K_freq = list()
exp_Ky = list()
for K in Ks:
    print(f"K = {K:1.1f}")
    y0_freq = list()
    exp_freq = list()
    labels = list()
    for soa_rnge in soa_rnges:
        label = f"{1/np.mean(soa_rnge):1.1f} Hz"
        labels.append(label)
        # generate new signals for each experiment
        trials, exp_noise = generate_trials(soa_rnge, ntrials, sig_perc=0.2)
        signals, probe_frame = signals_from_trials(trials, dt)
        yfreq_outs = list()
        exp_yfreq = list()
        for y0 in y0s:
            PARAMS_DICT["y0"] = y0
            print(f"Freq = {label}, y0 = {y0:1.2f}")
            ulst, vlst, ylst, Ilst, siglst, ylst2 = simulate_trial(
                ntrials=ntrials,
                nstages=None,
                duration=500,
                sigma=sigma,
                K=K,
                initI=initI,
                signal=signals,
            )
            # take y as output
            output = np.array(ylst)
            # record the y value at probe time, rhy time and soa time
            probe_frame["probe_out"] = output[
                probe_frame["index"], np.arange(output.shape[1])
            ]
            # GLM logit regresion
            # predicted response of the model based on decision threshold
            y = (probe_frame["probe_out"] >= PARAMS_DICT["y0"]).astype(int)
            slopes = list()
            exp_stats = list()
            # for each jitter type
            for jitter in ["rhy_jit", "soa_jit"]:
                if len(np.unique(y)) == 1:
                    slopes.append(np.nan)
                    continue
                X = probe_frame[jitter] * dt / 1000
                # normalize by average duration of that trial
                X /= np.array([np.mean(np.diff(trial)) for trial in trials])
                # add constant
                X = sm.add_constant(probe_frame[jitter] * dt / 1000)
                # fit the model
                mod = sm.GLM(y, X, family=sm.families.Binomial())
                try:
                    res = mod.fit()
                except PerfectSeparationError:
                    print("Perfect Separation Error...")
                    slopes.append(np.nan)
                else:
                    # take slope parameter
                    slopes.append(res.params[1])
                # get variance and mean of output at expectation
                exp_out = output[
                    np.round(probe_frame["index"] - probe_frame[jitter]).astype(int),
                    np.arange(output.shape[1]),
                ]
                exp_stats.append([np.mean(exp_out), np.std(exp_out)])
            yfreq_outs.append(slopes)
            exp_yfreq.append(exp_stats)
        y0_freq.append(yfreq_outs)
        exp_freq.append(exp_yfreq)
    y0K_freq.append(y0_freq)
    exp_Ky.append(exp_freq)

###############################################################################
#                              Plot the results.                              #
###############################################################################
# plot diff by performance
fig_dbyp, ax = plt.subplots()
amax = 100
for ind, y0_freq in enumerate(y0K_freq):
    slope_diffs = np.squeeze(np.diff(y0_freq, axis=2))
    for sc_ind, (perf, diff) in enumerate(
        zip(np.mean(y0_freq, axis=2).flatten(), slope_diffs.flatten())
    ):
        hdl = ax.plot(
            perf,
            diff,
            ".",
            ms=15,
            color=COLS[ind],
            alpha=0.4,
            zorder=np.random.normal(),
        )
        if sc_ind == 0:
            hdl[0].set_label(f"K = {Ks[ind]: 1.1f}")
ax.set_xlabel("Overall Performance")
ax.set_ylabel("SOA - RHY\nSOA is Positive")
ax.set_xlim([0, amax])
ax.set_ylim([-amax, amax])
ax.axhline(0, color=COLS[-1], ls="--")
plt.tight_layout()
plt.legend()
fig_dbyp.show()

# plot slope diffs by parameters
fig_dbypar, axs = plt.subplots(ncols=len(Ks), figsize=(15, 5))
vmax = 50
for ind, y0_freq in enumerate(y0K_freq):
    slope_diffs = np.squeeze(np.diff(y0_freq, axis=2))
    # vmax = np.nanmax(np.abs(slope_diffs))
    im = axs[ind].pcolormesh(
        y0s, freqs, slope_diffs, cmap="RdBu_r", vmin=-vmax, vmax=vmax, shading="auto"
    )
    axs[ind].set_title(f"K = {Ks[ind]:1.1f}")
fig_dbypar.supylabel("Frequency (Hz)")
fig_dbypar.supxlabel("Decision Threshold ($y_0$)")
fig_dbypar.suptitle("Discriminability (Slope)")
plt.tight_layout(rect=[0.05, 0.05, 0.95, 0.95])
cb = fig_dbypar.colorbar(im, ax=axs)
cb.ax.set_title("SOA - RHY")
fig_dbypar.show()

################################################################################
#                                 Save Figures                                 #
################################################################################
if savefigs:
    fig_dbyp.savefig(fig_dir + "SAM_ThrFq_diff_by_perf.png")
    fig_dbypar.savefig(fig_dir + "SAM_ThrFq_diff_by_params.png")
