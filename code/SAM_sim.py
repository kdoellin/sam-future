"""Simulating Jazayeri's SAM Model of temporal prediction."""
import matplotlib.pyplot as plt
import numpy as np

# Original value: u = 0.7, v = 0.2
PARAMS_DICT = {
    "Wut": 6,
    "Wuv": 6,
    "Wvt": 6,
    "Wvu": 6,
    "dt": 10,
    "tau": 100,
    "y0": 0.7,
    "IF": 50,
    "uinit": 0.7,
    "vinit": 0.2,
    "yinit": 0.5,
    "first_duration": 750,
    "regime": 0,
}

# COLS = (
#     np.array(
#         [[226, 155, 80], [28, 49, 68], [191, 33, 30], [141, 167, 190], [159, 184, 173]]
#     )
#     / 256
# )

COLS = (
    np.array(
        [[194, 30, 80], [50, 192, 241], [193, 147, 46], [141, 167, 190], [159, 184, 173]]
    )
    / 256
)

def thresh_exp(x):
    """Sigmoid non-linearity."""
    return 1 / (1 + np.exp(-x))


def start_simulation_parallel(state_init, params, K, sigma, niter, regime, signal=None):
    """Run simulation over multiple trials for a period.

    Inputs:
    state_init: an array which includes:
        * I: initial current
        * u: initial state of u
        * v: initial state of v
        * y: initial state of y (readout neuron)
        * sig: state indicator (0 or 1)

    params: a dictionary of relevant parameters of the network, see PARAMS_DICT
    niter: number of iterations
    signal (2d array): an array the length of niter containing the entirety of s-values
        for the run. Must be 2D even if there is only one signal.

    Outputs: each list contains niter elements
    u_lst: list of u activities
    v_lst: list of v activities
    y_lst: list of y activities
    I_lst: list of I activities
    sig_lst: list of sig in this simulation

    regime: 0 = intermediate I, 1 = low I regime, 2 = high I regime

    """
    # signal is the entire time course of s, must be 0 or 1
    if signal is not None:
        if niter is None:
            niter = len(signal)
        assert np.in1d(np.unique(signal), np.array([0, 1])).all()
        assert len(signal) == niter
        # save K value elsewhere so you can reset K to zero and back
        K_orig = K
        K = K_orig * np.ones(signal.shape[1])
    # Unpack parameters of the simulation
    Wut = params["Wut"]
    Wuv = params["Wuv"]
    Wvt = params["Wvt"]
    Wvu = params["Wvu"]
    dt = params["dt"]
    tau = params["tau"]
    IF = params["IF"]
    y0 = params["y0"]  # The target (threshold) value of y

    # Unpack variables
    I, u, v, y, sig = state_init
    ntrials = len(I)

    I = I.copy()
    u = u.copy()
    v = v.copy()
    y = y.copy()

    sig_lst = []
    u_lst = []
    v_lst = []
    y_lst = []
    I_lst = []

    sig_counter = 0

    for i in range(niter):
        if signal is not None:
            sig = signal[i]
            sig_counter += sig
            # set K to zero on first input as first time is random.
            K[sig_counter == 1] = 0
            K[sig_counter > 1] = K_orig
        # Update I, u, v and y
        if regime == 0 or regime == 2:
            I += (sig * K * (y - y0)) / tau * dt
        elif regime == 1:
            I -= (sig * K * (y - y0)) / tau * dt

        if regime == 0 or regime == 1:
            u += (
                (
                    -u
                    + thresh_exp(
                        Wut * I - Wuv * v - sig * IF + np.random.randn(ntrials) * sigma
                    )
                )
                / tau
                * dt
            )
            v += (
                (
                    -v
                    + thresh_exp(
                        Wvt * I - Wvu * u + sig * IF + np.random.randn(ntrials) * sigma
                    )
                )
                / tau
                * dt
            )
            y += (-y + u - v + np.random.randn(ntrials) * sigma) / tau * dt
        elif regime == 2:
            u += (
                (
                    -u
                    + thresh_exp(
                        Wut * I - Wuv * v + sig * IF + np.random.randn(ntrials) * sigma
                    )
                )
                / tau
                * dt
            )
            v += (
                (
                    -v
                    + thresh_exp(
                        Wvt * I - Wvu * u - sig * IF + np.random.randn(ntrials) * sigma
                    )
                )
                / tau
                * dt
            )
            y += (-y + u - v + np.random.randn(ntrials) * sigma) / tau * dt

        v_lst.append(v.copy())
        u_lst.append(u.copy())
        y_lst.append(y.copy())
        I_lst.append(I.copy())
        sig_lst.append(sig)

    return u_lst, v_lst, y_lst, I_lst, sig_lst


def plot_simulation_parallel(ulst, vlst, ylst, Ilst, siglst, params_dict):
    """Plot the simulations."""
    dt = params_dict["dt"]
    nsteps = len(ulst)
    tlst = np.arange(nsteps).astype("int")
    sig_lst = np.floor(tlst / 100) % 2
    sig_lst = sig_lst.astype("int")

    ulst_arr = np.array(ulst)
    vlst_arr = np.array(vlst)
    ylst_arr = np.array(ylst)
    Ilst_arr = np.array(Ilst)

    wherelst = np.array(siglst) == 1.0
    wherelst_shifted = np.concatenate((wherelst[1:], [False]))
    wherelst += wherelst_shifted

    # wherelst2 = np.array(siglst) == 2.0

    fig, ax = plt.subplots(4, 1, figsize=(10, 10))
    ax[0].plot(np.arange(nsteps) * dt, ulst_arr, "b", alpha=0.2)
    ax[0].set_ylabel("u")
    ax[0].fill_between(
        np.arange(nsteps) * dt,
        np.min(np.array(ulst)),
        np.max(np.array(ulst)),
        where=wherelst,
        alpha=0.5,
    )

    ax[1].plot(np.arange(nsteps) * dt, vlst_arr, "b", alpha=0.2)
    ax[1].set_ylabel("v")
    ax[1].fill_between(
        np.arange(nsteps) * dt,
        np.min(np.array(vlst)),
        np.max(np.array(vlst)),
        where=wherelst,
        alpha=0.5,
    )

    ax[2].plot(np.arange(nsteps) * dt, ylst_arr, "b", alpha=0.2)
    ax[2].set_ylabel("y")
    ax[2].fill_between(
        np.arange(nsteps) * dt,
        np.min(np.array(ylst)),
        np.max(np.array(ylst)),
        where=wherelst,
        alpha=0.5,
    )
    ax[2].hlines(PARAMS_DICT["y0"], 0, nsteps * dt, linestyle="dotted")

    ax[3].plot(np.arange(nsteps) * dt, Ilst_arr, "b", alpha=0.2)
    ax[3].set_ylabel("I")
    ax[3].fill_between(
        np.arange(nsteps) * dt,
        np.min(np.array(Ilst)),
        np.max(np.array(Ilst)),
        where=wherelst,
        alpha=0.5,
    )
    ax[3].set_xlabel("Time (ms)")


# Functions for using in a for loop
def get_times_lst_from_y(ylst):
    """Model readout based on y.

    Input:
    - ylst: np array of y activations

    Output:
    A 1d array of the times to threshold of the last behavior
    """
    ndiscard = 10
    threshold = PARAMS_DICT["y0"]
    # Discard first 10 samples
    ylst = ylst[ndiscard:]
    times_lst = []
    ntrials = ylst.shape[1]

    # Find time to act
    for k in range(ntrials):
        # Check if the bound is reached (sometimes it's not!)
        if np.max(ylst[:, k]) > threshold:
            times_lst.append(np.nonzero(ylst[:, k] > threshold)[0][0])
        else:
            times_lst.append(np.inf)
            # print('Bound not reached')

    return np.array(times_lst) + ndiscard


def simulate_trial(ntrials, duration, nstages, sigma, K, initI, signal=None, y0=None):
    """Simulate a complete trial."""
    # Initial run
    first_duration = PARAMS_DICT[
        "first_duration"
    ]  # duration in ms of first duration (500 ms + exponential with mean 250)
    regime = PARAMS_DICT["regime"]

    nbin = int(duration / PARAMS_DICT["dt"])
    nbinfirst = int(first_duration / PARAMS_DICT["dt"])

    uinit = PARAMS_DICT["uinit"]
    vinit = PARAMS_DICT["vinit"]
    yinit = PARAMS_DICT["yinit"]

    state_init = [
        np.ones(ntrials) * initI,
        np.ones(ntrials) * uinit,
        np.ones(ntrials) * vinit,
        np.ones(ntrials) * yinit,
        0.0,
    ]

    if signal is None:
        ulst, vlst, ylst, Ilst, siglst = start_simulation_parallel(
            state_init, PARAMS_DICT, 0, sigma, nbinfirst, regime
        )

        # For subsequent runs, flip the state every 100 trials
        for k in range((nstages - 2) * 2):

            # acoefs = 1 - acoefs
            state_init = [
                Ilst[-1],
                ulst[-1],
                vlst[-1],
                ylst[-1],
                (state_init[4] + 1.0) % 2,
            ]
            # print('k = ', k, 'state_init[4] =', state_init[4])
            if state_init[4] == 0.0:
                ulst2, vlst2, ylst2, Ilst2, siglst2 = start_simulation_parallel(
                    state_init, PARAMS_DICT, K, sigma, nbin, regime
                )
            else:
                if k == 0:
                    # No update for first flash
                    ulst2, vlst2, ylst2, Ilst2, siglst2 = start_simulation_parallel(
                        state_init, PARAMS_DICT, 0, sigma, 1, regime
                    )
                else:
                    ulst2, vlst2, ylst2, Ilst2, siglst2 = start_simulation_parallel(
                        state_init, PARAMS_DICT, K, sigma, 1, regime
                    )

            ulst += ulst2
            vlst += vlst2
            ylst += ylst2
            Ilst += Ilst2
            siglst += siglst2

        if nstages > 1:
            state_init = [
                Ilst[-1],
                ulst[-1],
                vlst[-1],
                ylst[-1],
                (state_init[4] + 1.0) % 2,
            ]

            if nstages == 2:
                Keff = 0
            else:
                Keff = K

            # For the last run, produce the behavior when the threshold is reached
            ulst2, vlst2, ylst2, Ilst2, siglst2 = start_simulation_parallel(
                state_init, PARAMS_DICT, Keff, sigma, 1, regime
            )

            ulst += ulst2
            vlst += vlst2
            ylst += ylst2
            Ilst += Ilst2
            siglst += siglst2

            state_init = [
                Ilst[-1],
                ulst[-1],
                vlst[-1],
                ylst[-1],
                (state_init[4] + 1.0) % 2,
            ]
            # For the last run, produce the behavior when the threshold is reached
            ulst2, vlst2, ylst2, Ilst2, siglst2 = start_simulation_parallel(
                state_init, PARAMS_DICT, K, sigma, nbin * 2, regime
            )

            ulst += ulst2
            vlst += vlst2
            ylst += ylst2
            Ilst += Ilst2

            siglst2[nbin] = 1
            siglst += siglst2
        else:
            print(len(siglst))
            siglst[-1] = 1
            ylst2 = ylst
    else:
        if y0 is not None:
            PARAMS_DICT["y0"] = y0
        ulst, vlst, ylst, Ilst, siglst = start_simulation_parallel(
            state_init, PARAMS_DICT, K, sigma, len(signal), regime, signal
        )
        ylst2 = ylst

    return ulst, vlst, ylst, Ilst, siglst, ylst2
