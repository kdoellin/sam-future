# Behavior by Frequency and Threshold

Testing which parameters can turn SAM from SOA to Rhythm method.

## Framework

We have shown that the oscillator can shift well between SOA and Rhythm types of behavior.
Do other models perform similarly?
What does it take to generate behavior like humans, where doing well means doing SOA and doing poorly means doing Rhythm.
Earlier I ran a basic analysis of the Jazlab Simulation and found something kind of surprising.
I roved the learning parameter $K$ and the amount of noise $\sigma$ in each frequency space consistent with our experiment to see how this variation would lead to changes in method preference by overall performance.

The model does SOA at 1.2 and 2 Hz (for all levels of performance) and RHY for 4 Hz (again for all levels of performance).
This is sort of human like when you consider our initial bar graphs,
but it doesn't explain the performance effects and therefore is a bit jarring.

![](../figures/SAM_Freq_diff_by_perf.png)

So what is driving this difference across frequencies?
Well it turns out the difference has to do with the current which drives the models predictions.
They use the current $I$ to control the speed of the prediction ramp.
However, it also changes where the ramp plateaus.

![](../figures/SAM_plateau_by_current.png)

This figure shows the results of two different currents $I = 0.77$ (as in the paper) and $I = 0.65$,
and the resulting values for $u$ (left), $v$ (middle) and $y$ (right), which is the difference between them ($y$ is also our ramp.).
You can see that the threshold is in a very different part of the curve and that when we have lower current (faster stim rate) we have a more sensitive part of the model.

**My hypothesis is that this shift in the threshold relative to the plateau is what drives the change in behavior.**
If it is true, then we should see that changing threshold should change the diving line between frequencies that are SOA and those that are rhythm.

## Result

In this analysis I did a more detailed analysis of how frequency affects method preference.
I limited myself to four representative values of $K$ and one value of $\sigma$.
I roved over stimulus frequency (from .5 Hz to 5 Hz) and $y_0$ threshold (from 0.3 to 0.9)
and plotted the difference in slope for SOA vs Rhythm method.

![](../figures/SAM_ThrFq_diff_by_params.png)

The first thing to notice is that my hypothesis was definitely correct.
Changing the threshold, changes where you shift from blue (prefer rhythm) to red (prefer soa).
However, we also see that K has an effect as well.
Increasing K (how much you alter the current based on your prediction error), affects the size and strength of the SOA regime (and possibly also the RHY regime).

Given this, it's likely that there is a possible set of parameters that could be fit to our participant data.
This model is more complex and has more moving pieces that could be used to better fit the data.
The question is will this extra flexibility be worthwhile.
**TODO**: A direct comparison of models against participant behavior is definitely worthwhile.

## Thoughts
I'm trying to decide if this relationship between plateau and ramp speed (and therefore temporal prediction) is inherent to the model.
I think it is given this construction.
The current provided to both $u$ and $v$ is inhibitory.
The more it increases the more it slows the activity to each point such it stabilizes at any "earlier" point than it otherwise would have.
When you look at figure 2, you can see that the increase in speed in y seems more or less proportional to the increase plateau.
I think the increase in speed is directly the result of the change in gain.
Therefore, to build a model that changes speed without changing plateau, you need entirely new circuitry.
This is an effect that is inherent to the model.
