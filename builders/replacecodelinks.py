"""Replace Links to Code with links to markdown version."""
import glob
import re

reports = glob.glob("reports/*.md")
reports += glob.glob("ideas/*.md")

for report in reports:
    with open(report, "r") as f:
        s = f.read()
    news = re.sub(r"\[(.+)\]\((.*)\.py\)", r"[\1](\2.md)", s)
    news = re.sub(r"\[(.+)]\:\s(.*)\.py\n", r"[\1]: \2.md\n", news)
    with open(report, "w") as f:
        f.write(news)
