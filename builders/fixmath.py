"""Adjust latex syntax to MathJax syntax."""
import glob
import re

reports = glob.glob("reports/*.md")
reports += glob.glob("ideas/*.md")

for report in reports:

    with open(report, "r") as f:
        s = f.read()
    news = re.sub(r"\\\[(.*?)\\\]", r"\\\\[\1\\\\]", s)
    news = re.sub(r"\$(.*?[^\s])\$", r"\\\\( \1 \\\\)", news)
    with open(report, "w") as f:
        f.write(news)
