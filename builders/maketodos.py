"""Make a list of todos from all of the reports."""
import glob
import re


def linkify(s):
    """Make section headers into anchor links."""
    title = re.findall(r"#+\s(.+)$", s)[0]
    title = re.sub(r"[^\w\s]", "", title.lower())
    title = re.sub(r"\s+", "-", title)
    return title


# bold TODO is the search term
search = "**TODO**:"
reports = glob.glob("reports/*.md")
reports += glob.glob("ideas/*.md")

head_dir = "headers/"
td_filename = f"{head_dir}TODO.md"
with open(td_filename, "w") as td_content:
    td_content.write("## TODO List\n")
    td_content.write(
        "\n\nHere is a list of possible TODOs in case you are wondering"
        " what to do next\n"
    )
# write links to todo file in Summary and README
with open("SUMMARY.md", "a") as summ:
    summ.write("- [TODO List]({})\n".format(td_filename))

with open("README.md", "a") as readme:
    readme.write(
        f"\n\n# TODO List\nA TODO list is automatically generated "
        f"[here]({td_filename}"
        f") for your perusal.\n"
    )

count = 0
# now write the todos to file
for report in reports:
    # read report contents
    with open(report, "r") as f:
        s = f.read()
    # search for todos
    if search in s:
        # split each section by header locations
        sections = s.split("\n#")
        # page title
        title = re.findall(r"#+\s(.*)", s.splitlines()[0])[0]
        with open(td_filename, "a") as td_content:
            td_content.write("#### {}\n".format(title))
            for section in sections:
                if search in section:
                    # get a location link from the headline
                    lines = section.splitlines()
                    header = linkify(lines[0])

                    for ind, line in enumerate(lines):
                        if search in line:
                            start = line.index(search)
                            done = line[start - 1] == line[start + len(search)] == "~"
                            mark = "x" if done else " "
                            editline = line.replace(search, "").replace("~", "")
                            followline = (
                                lines[ind + 1]
                                if len(lines) > ind + 1
                                and lines[ind + 1].startswith("(**DONE**")
                                else ""
                            )
                            td_content.write(
                                f"{count}. [{mark}]"
                                f" [{editline}]({'../' + report}#{header})"
                                f" {followline}\n"
                            )
                            count += 1
