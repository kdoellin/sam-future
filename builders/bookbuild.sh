#!/bin/bash

# read in environment variable
export CI_REPOSITORY_URL
# read title and build book with it
line=$(head -n 1 README.md)
printf "n\n${line: 2}" | mdbook init build
# add in other book settings
printf "\n[output.html]\nmathjax-support = true\ngit-repository-url = '${CI_REPOSITORY_URL}'\ngit-repository-icon = 'fa-gitlab'\n" >> build/book.toml
printf '\n[output.html.fold]\nenable = true\nlevel = 0' >> build/book.toml
# remove boilerplate src and copy in our own files
rm -rf build/src
mkdir build/src
for file in ./*; do
    # pick only directories and Markdown Files
    if [[ (-d "$file" || "$file" == *.md) ]];
    then
        # Move theme to build folder
        if [[ ("${file}" == *theme*) ]];
        then
            cp -r $file build/
        # only move code files (.py) to code folder
        elif [[ "$file" == *code* ]];
        then
            mkdir build/src/code/
            cp -r $file/*.py build/src/code/
            cp -r $file/*.md build/src/code/
        # don't touch anything labeled build or cargo.
        elif [[ ("$file" != *build*) && ("$file" != *cargo*) ]];
        then
            cp -r $file build/src
        fi
    fi
done
