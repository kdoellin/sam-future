"""Append to readme based on contents of reports."""
import glob
import re

reports = glob.glob("reports/*.md")

with open("README.md", "a") as rdme:
    rdme.write("\n## Reports\n")

with open("SUMMARY.md", "w+") as summ:
    summ.write("# Summary\n\n- [REPORTS](README.md)\n")

for report in reports:
    file = open(report, "r")
    s = file.read()
    title = re.findall(r"^#\s(.*?)$", s, re.MULTILINE + re.DOTALL)[0]
    descr = re.findall(title + r"\n+(.*?)\n+#", s, re.MULTILINE + re.DOTALL)[0]
    with open("README.md", "a") as rdme:
        rdme.write(f"- [{title}]({report}): {descr}\n")

    with open("SUMMARY.md", "a") as summ:
        summ.write(f"   - [{title}]({report})\n")
