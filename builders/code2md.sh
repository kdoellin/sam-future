#!/bin/bash

srcdir="./"
headdir="headers/"
srcheaddir="${srcdir}${headdir}"
codedir="${srcdir}code/"
SUMMARY="${srcdir}SUMMARY.md"
CODE="${srcheaddir}CODE.md"
TEST="${srcheaddir}TESTS.md"
SUPPORT="${srcheaddir}SUPPORT.md"
echo "- [CODE](${headdir}CODE.md)" >> $SUMMARY
echo "  - [TESTS](${headdir}TESTS.md)" >> $SUMMARY

# Handle processing of test scripts.
printf "The following are all of the TEST scripts used to generate figures\n" > $TEST
for file in "$codedir"test*.py
do
    if [ -f $file ]; then
        nosrc="${file#src/}"
        printf "\`\`\`python\n{{#include ../%s}}\n\`\`\`" "$nosrc" > "${file%.py}.md"
        echo "      - [${file##*/}](${nosrc%.py}.md)" >> $SUMMARY
        echo "  - [${file##*/}](../${nosrc%.py}.md)" >> $TEST
    fi
done

# Handle processing of support scripts
printf "The following are all of the SUPPORT Functions used to generate the scripts\n"\
    > $SUPPORT
echo "  - [SUPPORT FUNCTIONS](${headdir}SUPPORT.md)" >> $SUMMARY
for file in "$codedir"*.py
do
    if [[ $file != *"test"* ]]; then
        nosrc="${file#src/}"
        printf "\`\`\`python\n{{#include ../%s}}\n\`\`\`" "$nosrc" > "${file%.py}.md"
        echo "      - [${file##*/}](${nosrc%.py}.md)" >> $SUMMARY
        echo "  - [${file##*/}](../${nosrc%.py}.md)" >> $SUPPORT
    fi
done

# create code directory to support it all.
printf "# Code Repository\n" > $CODE
printf "The following is the code used to generate these scripts.\n" >> $CODE
printf "It is broken up into:\n" >> $CODE
printf "%s\n" "- [TESTS](TESTS.md): the main scripts that generate the figures" >> $CODE
printf "%s\n" "- [SUPPORT FUNCTIONS](SUPPORT.md): the functions used in the analysis." >> $CODE
