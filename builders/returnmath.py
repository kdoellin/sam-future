"""Adjust MathJax syntax back to Latex syntax."""
import glob
import re

reports = glob.glob("src/reports/*.md")
reports += glob.glob("src/ideas/*.md")

for report in reports:

    with open(report, "r") as f:
        s = f.read()
    news = re.sub(r"\\\\\[(.*?)\\\\\]", r"\\[\1\\]", s)
    news = re.sub(r"\\\\\(\s(.*?)\s\\\\\)", r"$\1$", news)
    with open(report, "w") as f:
        f.write(news)
